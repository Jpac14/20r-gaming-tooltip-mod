package io.jpac14.tooltipmod.config;

public enum ConfigErrorCode {
    NOFILE,
    INVALIDJSON,
    VALID
}
