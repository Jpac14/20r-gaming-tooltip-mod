package io.jpac14.tooltipmod.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import io.jpac14.tooltipmod.TooltipMod;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TooltipConfig {
    public static HashMap<String, String> jsonObj;
    
    private static final String configFilePath = "./config/tooltipmod/config.json";
    private static Gson gson;
    
    /*
     * Initalises configuration file
     * Checks the configuration file
     * 	a. If the file doesn't exist, creates one
     * 	b. If the JSON is invalid, exits
     * 	c. If valid, reads JSON
     */
    public static void initConfig() throws FileNotFoundException, IOException {
	gson = new Gson();
	
	ConfigErrorCode code;
	
	if ((code = checkConfig()) == ConfigErrorCode.VALID) {
            JsonReader reader = new JsonReader(new FileReader(configFilePath));
            	    
            jsonObj = gson.fromJson(reader, HashMap.class);   
	} else if (code == ConfigErrorCode.NOFILE) {
	    createConfig();
	} else if (code == ConfigErrorCode.INVALIDJSON) {
	    TooltipMod.logger.error("Invalid JSON configuration file");
	    
	    FMLCommonHandler.instance().exitJava(1, true);
	}
    }
    
    /*
     * Checks the configuration file
     * 1. Checks if the configuration file exists
     * 2. Checks if it contains valid JSON
     */
    private static ConfigErrorCode checkConfig() throws FileNotFoundException {
	File configFile = new File(configFilePath);
	
	if (configFile.exists()) {  
	    try {
		JsonReader reader = new JsonReader(new FileReader(configFilePath));

		gson.fromJson(reader, Object.class);
		
		return ConfigErrorCode.VALID;
	    } catch (com.google.gson.JsonSyntaxException exception) {
		return ConfigErrorCode.INVALIDJSON;
	    }
	}
	
	return ConfigErrorCode.NOFILE;
    }
    
    /*
     * Create a configuration file
     * 1. Create the directory and file for the file
     * 2. Writes a empty hashmap to the file
     */
    private static void createConfig() throws IOException {
	File configFile = new File(configFilePath);
	configFile.getParentFile().mkdir();
	configFile.createNewFile();
	
	FileWriter fileWriter = new FileWriter(configFilePath);
	fileWriter.write("{}");
	fileWriter.close();
    }
}
