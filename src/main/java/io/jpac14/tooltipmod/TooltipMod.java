package io.jpac14.tooltipmod;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.logging.log4j.Logger;

import io.jpac14.tooltipmod.command.TooltipCommand;
import io.jpac14.tooltipmod.config.TooltipConfig;
import io.jpac14.tooltipmod.event.TooltipItemPickupEvent;

@Mod(modid = TooltipMod.MODID, name = TooltipMod.NAME, version = TooltipMod.VERSION, serverSideOnly = true, acceptableRemoteVersions = "*")
public class TooltipMod {
    public static final String MODID = "tooltipmod";
    public static final String NAME = "Tooltip Mod for 20r Gaming";
    public static final String VERSION = "1.3";

    public static Logger logger;
    
    /*
     * Pre-initalisation event, set logger, initalise configuration
     */
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) throws FileNotFoundException, IOException {
        logger = event.getModLog();
        
        TooltipConfig.initConfig();
    }
    
    /*
     * Initalisation event
     */
    @EventHandler
    public void init(FMLInitializationEvent event) {	
	MinecraftForge.EVENT_BUS.register(new TooltipItemPickupEvent());
    }
    
    /*
     * Server starting event, register tooltip command
     */
    @EventHandler
    public void serverStarting(FMLServerStartingEvent event) {	
	event.registerServerCommand(new TooltipCommand());
    }
}
