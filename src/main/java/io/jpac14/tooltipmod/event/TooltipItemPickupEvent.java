package io.jpac14.tooltipmod.event;

import io.jpac14.tooltipmod.config.TooltipConfig;
import io.jpac14.tooltipmod.helper.TooltipHelper;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.event.entity.player.EntityItemPickupEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class TooltipItemPickupEvent {
    
    /*
     * Called when player pickups a item
     * Checks if there is a tooltip for item, if so sends tooltip
     */
    @SubscribeEvent
    public void onItemPickup(EntityItemPickupEvent event) {
	String itemRegistryName = event.getItem().getItem().getItem().getRegistryName().toString();
	
	if (TooltipHelper.checkConfigForItem(itemRegistryName)) {
	    TextComponentString message = new TextComponentString(
		    String.format("§3There is a tooltip for %s, §r%s", 
		    event.getItem().getItem().getDisplayName(),
		    TooltipConfig.jsonObj.get(itemRegistryName)));
	    
	    event.getEntityPlayer().sendMessage(message);
	}
    }
}
