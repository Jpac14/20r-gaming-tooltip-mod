package io.jpac14.tooltipmod.helper;

import io.jpac14.tooltipmod.config.TooltipConfig;

public class TooltipHelper {
    /*
     * Checks if the configuration file has a tooltip for the item requested
     */
    public static boolean checkConfigForItem(String playerBlock) {
	try {
	    if (TooltipConfig.jsonObj.containsKey(playerBlock)) {
		return true;
	    } else {
		return false;
	    }
	} catch (NullPointerException exception) {
	    return false;
	}
    }
}
