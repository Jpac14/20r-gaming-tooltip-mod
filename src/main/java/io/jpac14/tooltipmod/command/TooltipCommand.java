package io.jpac14.tooltipmod.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import io.jpac14.tooltipmod.config.TooltipConfig;
import io.jpac14.tooltipmod.helper.TooltipHelper;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;

public class TooltipCommand extends CommandBase {
    /*
     * Returns the name for the command
     */
    @Override
    public String getName() {
	return "tooltip";
    }
    
    /*
     * Returns the usage
     */
    @Override
    public String getUsage(ICommandSender sender) {
	return "/tooltip <item>";
    }
    
    /*
     * Returns a list of aliases
     */
    @Override
    public List<String> getAliases() {
	List<String> aliases = new ArrayList<String>();
	
	aliases.add("t");
	aliases.add("tool");
	aliases.add("info");
	
	return aliases;
    }
    
    /*
     * Get a list of options for when the user presses the TAB key
     */
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
	return args.length == 1 ? getListOfStringsMatchingLastWord(args, Item.REGISTRY.getKeys()) : Collections.emptyList();
    }
    
    /*
     * Gets the item the player is requesting, either from arguments or main hand
     */
    private static String getPlayerItem(ICommandSender sender, String[] args) {
	if (args.length == 1) {
	    return args[0];
	} else { 
	    // EntityPlayerMP has the function to access player hand
	    EntityPlayerMP player = (EntityPlayerMP) sender;
	    
	    return player.getHeldItemMainhand().getItem().getRegistryName().toString();
	}
    }
    
    /*
     * Called when command is executed
     * 1. Gets the item the player requested
     * 2. Checks if the item is in the configuration file, if so sends tooltip, if not sends not found message
     */
    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
	if (args.length > 1) {
	    throw new WrongUsageException("/tooltip <item>");
	}
	
	String playerBlock = getPlayerItem(sender, args);
	
	if (TooltipHelper.checkConfigForItem(playerBlock)) {
	    TextComponentString message = new TextComponentString(TooltipConfig.jsonObj
		    .get(playerBlock));
	    sender.sendMessage(message); 
	} else {
	    TextComponentString message = new TextComponentString("§4No tooltip for this item");
	    sender.sendMessage(message); 
	}
    }

}
